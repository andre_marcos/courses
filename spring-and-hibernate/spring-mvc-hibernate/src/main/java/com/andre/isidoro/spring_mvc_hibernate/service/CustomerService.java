package com.andre.isidoro.spring_mvc_hibernate.service;

import java.util.List;

import com.andre.isidoro.spring_mvc_hibernate.entity.Customer;

public interface CustomerService {
	
	public List<Customer> getCustomers();

}
